
--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `color` varchar(30) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colors`
--

LOCK TABLES `colors` WRITE;
/*!40000 ALTER TABLE `colors` DISABLE KEYS */;
INSERT INTO `colors` VALUES (1,'AliceBlue'),(2,'AntiqueWhite'),(3,'Aqua'),(4,'Aquamarine'),(5,'Azure'),(6,'Beige'),(7,'Bisque'),(8,'Black'),(9,'BlanchedAlmond'),(10,'Blue'),(11,'BlueViolet'),(12,'Brown'),(13,'BurlyWood'),(14,'CadetBlue'),(15,'Chartreuse'),(16,'Chocolate'),(17,'Coral'),(18,'CornflowerBlue'),(19,'Cornsilk'),(20,'Crimson'),(21,'Cyan'),(22,'DarkBlue'),(23,'DarkCyan'),(24,'DarkGoldenRod'),(25,'DarkGray'),(26,'DarkGrey'),(27,'DarkGreen'),(28,'DarkKhaki'),(29,'DarkMagenta'),(30,'DarkOliveGreen'),(31,'Darkorange'),(32,'DarkOrchid'),(33,'DarkRed'),(34,'DarkSalmon'),(35,'DarkSeaGreen'),(36,'DarkSlateBlue'),(37,'DarkSlateGray'),(38,'DarkSlateGrey'),(39,'DarkTurquoise'),(40,'DarkViolet'),(41,'DeepPink'),(42,'DeepSkyBlue'),(43,'DimGray'),(44,'DimGrey'),(45,'DodgerBlue'),(46,'FireBrick'),(47,'FloralWhite'),(48,'ForestGreen'),(49,'Fuchsia'),(50,'Gainsboro'),(51,'GhostWhite'),(52,'Gold'),(53,'GoldenRod'),(54,'Gray'),(55,'Grey'),(56,'Green'),(57,'GreenYellow'),(58,'HoneyDew'),(59,'HotPink'),(60,'IndianRed'),(61,'Indigo'),(62,'Ivory'),(63,'Khaki'),(64,'Lavender'),(65,'LavenderBlush'),(66,'LawnGreen'),(67,'LemonChiffon'),(68,'LightBlue'),(69,'LightCoral'),(70,'LightCyan'),(71,'LightGoldenRodYellow'),(72,'LightGray'),(73,'LightGrey'),(74,'LightGreen'),(75,'LightPink'),(76,'LightSalmon'),(77,'LightSeaGreen'),(78,'LightSkyBlue'),(79,'LightSlateGray'),(80,'LightSlateGrey'),(81,'LightSteelBlue'),(82,'LightYellow'),(83,'Lime'),(84,'LimeGreen'),(85,'Linen'),(86,'Magenta'),(87,'Maroon'),(88,'MediumAquaMarine'),(89,'MediumBlue'),(90,'MediumOrchid'),(91,'MediumPurple'),(92,'MediumSeaGreen'),(93,'MediumSlateBlue'),(94,'MediumSpringGreen'),(95,'MediumTurquoise'),(96,'MediumVioletRed'),(97,'MidnightBlue'),(98,'MintCream'),(99,'MistyRose'),(100,'Moccasin'),(101,'NavajoWhite'),(102,'Navy'),(103,'OldLace'),(104,'Olive'),(105,'OliveDrab'),(106,'Orange'),(107,'OrangeRed'),(108,'Orchid'),(109,'PaleGoldenRod'),(110,'PaleGreen'),(111,'PaleTurquoise'),(112,'PaleVioletRed'),(113,'PapayaWhip'),(114,'PeachPuff'),(115,'Peru'),(116,'Pink'),(117,'Plum'),(118,'PowderBlue'),(119,'Purple'),(120,'Red'),(121,'RosyBrown'),(122,'RoyalBlue'),(123,'SaddleBrown'),(124,'Salmon'),(125,'SandyBrown'),(126,'SeaGreen'),(127,'SeaShell'),(128,'Sienna'),(129,'Silver'),(130,'SkyBlue'),(131,'SlateBlue'),(132,'SlateGray'),(133,'SlateGrey'),(134,'Snow'),(135,'SpringGreen'),(136,'SteelBlue'),(137,'Tan'),(138,'Teal'),(139,'Thistle'),(140,'Tomato'),(141,'Turquoise'),(142,'Violet'),(143,'Wheat'),(144,'White'),(145,'WhiteSmoke'),(146,'Yellow'),(147,'YellowGreen');
/*!40000 ALTER TABLE `colors` ENABLE KEYS */;
UNLOCK TABLES;
